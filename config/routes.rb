Rails.application.routes.draw do
  get '/home', to: 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/contacto', to: 'static_pages#contacto'
  get '/video', to: 'static_pages#video'
  get '/comprar', to: 'static_pages#comprar'
  root 'static_pages#home'
  resources :posts
  resources :articles
  resources :recipes
end
