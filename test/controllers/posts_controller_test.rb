require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  test "should get posts index" do
    get posts_url
    assert_response :success
  end
  
   test "should get new post" do
    get new_post_url
    assert_response :success
  end
  
   # test "should edit posts" do
    #get edit_post_url
    #assert_response :success
  #end
  
   #test "should show post" do
    #get post_url
    #assert_response :success
  #end
end
