require 'test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest
 test "should get articles index" do
    get articles_url
    assert_response :success
  end
  
   test "should get new article" do
    get new_article_url
    assert_response :success
  end
end
