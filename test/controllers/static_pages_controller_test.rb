require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get home_url
    assert_response :success
  end

  test "should get about" do
    get about_url
    assert_response :success
  end

  test "should get contacto" do
    get contacto_url
    assert_response :success
  end

  test "should get video" do
    get video_url
    assert_response :success
  end

  test "should get comprar" do
    get comprar_url
    assert_response :success
  end

end
