class Recipe < ApplicationRecord
    has_many :ingredients, inverse_of: :recipe
    has_many :directions, inverse_of: :recipe
    has_one_attached :image
    accepts_nested_attributes_for :ingredients, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :directions, reject_if: :all_blank, allow_destroy: true
    validates :title, :description, :image, presence: true
    extend FriendlyId
    friendly_id :title, use: :slugged
end
