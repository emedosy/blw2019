class Article < ApplicationRecord
   validates :title, presence: true, length: { minimum: 5}
   validates :body, :image, presence: true
   has_one_attached :image
   extend FriendlyId
   friendly_id :title, use: :slugged
end