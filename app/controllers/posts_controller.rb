class PostsController < ApplicationController
 before_action :find_post, only: [:show, :edit, :update, :destroy]
 
  def index
    @posts = Post.all.order('created_at DESC')
  end
  
  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    
    if @post.save
      redirect_to @post, notice: "Success!"
    else
      render 'new'
    end
  end

  def update
    if @post.update(params[:post].permit(:title, :body, :image))
      redirect_to @post
    else
      render 'edit'
    end
  end

  def edit
  end

  def destroy
    @post.destroy
    
    redirect_to posts_path
  end

  def show
  end
  
  private
    def post_params
      params.require(:post).permit(:title, :body, :image)
    end
    
    def find_post
      @post = Post.friendly.find(params[:id])
    end
end
