class ArticlesController < ApplicationController
before_action :find_article, only:[:show, :edit, :update, :destroy]
    def index
        @articles =Article.all.order('created_at DESC')
    end
    
    def new
        @article = Article.new
    end
    
    def create
        @article = Article.new(article_params)
        if @article.save
            redirect_to @article
        else
            render 'new'
        end
    end
    
    def update
        if @article.update(params[:article].permit(:title, :body, :image))
            redirect_to @article
        else
            render 'edit'
        end
    end
    
    def edit
    end
    
    def destroy
        @article.destroy
        
        redirect_to articles_path
    end
    
    def show
    end
    
    private
        def article_params
            params.require(:article).permit(:title, :body, :image)
        end
        
        def find_article
            @article = Article.friendly.find(params[:id])
        end
end
