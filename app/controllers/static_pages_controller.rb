class StaticPagesController < ApplicationController
  def home
    @posts = Post.order("created_at desc").limit(3)
    @recipes =Recipe.order('created_at DESC').limit(2)
    @articles =Article.order('created_at DESC').limit(3).offset(3)
    @newarticles =Article.order('created_at DESC').limit(3)
    @bottomarticles =Article.order('created_at DESC').limit(1).offset(6)
  end

  def about
  end

  def contacto
  end

  def video
  end

  def comprar
  end
end
